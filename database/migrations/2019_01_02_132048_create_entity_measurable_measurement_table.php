<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityMeasurableMeasurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_measurable_measurement', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_measurable_id');
            $table->timestamp('timestamp')
                ->useCurrent()
                ->comment('Since measurements could be way more than a timespan, with a specified start and end, just assume a timestamp for when the measurement was taken, and a weight to differentiate between values.');
            $table->unsignedSmallInteger('weight')
                ->comment('Not sure how far this should go, but stick to smallint so we can store our 5-minute timespan properly.');
            $table->decimal('value', 12, 4);

            $table->foreign('entity_measurable_id')
                ->references('id')->on('entity_measurable')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_measurable', function($table)
        {
            $table->dropForeign(['entity_measurable_id']);
        });

        Schema::dropIfExists('entity_measurable_measurement');
    }
}
