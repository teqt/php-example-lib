<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityPingableHeartbeatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_pingable_heartbeat', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_pingable_id');
            $table->timestamp('timestamp')
                ->useCurrent();
            $table->boolean('value')
                ->comment('Pingables are just a true / false type. Let the type of pingable decide what assumtions should be made out of it. ');

            $table->foreign('entity_pingable_id')
                ->references('id')->on('entity_pingable')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_pingable_heartbeat', function($table)
        {
            $table->dropForeign(['entity_pingable_id']);
        });

        Schema::dropIfExists('entity_pingable_heartbeat');
    }
}
