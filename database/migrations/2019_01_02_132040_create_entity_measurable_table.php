<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityMeasurableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_measurable', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('measurable_id');

            $table->foreign('entity_id')
                ->references('id')->on('entity')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('measurable_id')
                ->references('id')->on('measurable')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_measurable', function($table)
        {
            $table->dropForeign(['entity_id', 'measurable_id']);
        });

        Schema::dropIfExists('entity_measurable');
    }
}
