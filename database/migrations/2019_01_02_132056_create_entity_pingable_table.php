<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityPingableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_pingable', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('pingable_id');

            $table->foreign('entity_id')
                ->references('id')->on('entity')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('pingable_id')
                ->references('id')->on('pingable')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_pingable', function($table)
        {
            $table->dropForeign(['entity_id', 'pingable_id']);
        });

        Schema::dropIfExists('entity_pingable');
    }
}
