<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')
                ->comment('There\'s probably more about this entity outside the scope of monitoring, identified by an unique identifier. I have chosen UUIDv4 as this identifier.');
            $table->string('display_name', 255)
                ->nullable()
                ->comment('Most likely not needed, for there would be a different source for this data.');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity');
    }
}
