<?php

namespace Monitor\Models\Entity\Measurable;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name for model
     * @var string
     */
    public $table = 'entity_measurable_measurement';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['entity_measurable_id', 'timestamp', 'weight', 'value'];

    /**
     * Transform timestamp values to DateTime objects
     * @param $value
     * @return \DateTime
     */
    final public function getTimestampAttribute($value):\DateTime
    {
        $datetime = new \DateTime($value);
        if(!$datetime->getTimestamp())
        {
            return null;
        }

        return $datetime;
    }
}