<?php

namespace Monitor\Models\Entity;

use Illuminate\Database\Eloquent\Model;

class Measurable extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name for model
     * @var string
     */
    public $table = 'entity_measurable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['entity_id', 'measurable_id'];
}