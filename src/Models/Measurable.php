<?php

namespace Monitor\Models;

use Illuminate\Database\Eloquent\Model;

class Measurable extends Model
{
    /**
     * Table name for model
     * @var string
     */
    public $table = 'measurable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'display_name'];
}