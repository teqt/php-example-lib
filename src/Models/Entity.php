<?php

namespace Monitor\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Monitor\Models\Entity\Measurable\Measurement;
use Monitor\Models\Entity\Pingable\Heartbeat;

class Entity extends Model
{
    /**
     * Table name for model
     * @var string
     */
    public $table = 'entity';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uuid', 'display_name'];

    /**
     * Local store for entity measurables
     *
     * @var Collection
     */
    protected $entityMeasurables;

    /**
     * Local store for entity pingables
     *
     * @var Collection
     */
    protected $entityPingables;

    /**
     * Get measurables by entity
     * @return Collection
     */
    final public function measurables(): Collection
    {
        $measurables = [];
        $entityMeasurables = $this->entityMeasurables();
        foreach ($entityMeasurables as $entityMeasurable)
        {
            $measurables[] = $entityMeasurable->measurable_id;
        }

        $measurables = array_unique($measurables);
        return Measurable::whereIn('id', $measurables)->get();
    }

    /**
     * Get pingables by entity
     * @return Collection
     */
    final public function pingables(): Collection
    {
        $pingables = [];
        $entityPingables = $this->entityPingables();
        foreach ($entityPingables as $entityPingable)
        {
            $pingables[] = $entityPingable->pingable_id;
        }

        $pingables = array_unique($pingables);
        return Pingable::whereIn('id', $pingables)->get();
    }

    /**
     * Get measurements by entity and measurable
     * @param Measurable $measurable
     * @return Collection
     */
    final public function measurements(Measurable $measurable, \DateTime $from = null, \DateTime $to = null): Collection
    {
        $measurements = [];
        $entityMeasurables = $this->entityMeasurables();
        foreach ($entityMeasurables as $entityMeasurable)
        {
            if ($measurable->id !== $entityMeasurable->measurable_id)
            {
                continue;
            }

            $measurements[] = $entityMeasurable->id;
        }

        $result = Measurement::whereIn('entity_measurable_id', $measurements);
        return $this->restrictResult($result, $from, $to);
    }

    /**
     * Get heartbeats by entity and pingable
     * @param Pingable $pingable
     * @return Collection
     */
    final public function heartbeats(Pingable $pingable, \DateTime $from = null, \DateTime $to = null): Collection
    {
        $heartbeats = [];
        $entityPingables = $this->entityPingables();
        foreach ($entityPingables as $entityPingable)
        {
            if ($pingable->id !== $entityPingable->pingable_id)
            {
                continue;
            }

            $heartbeats[] = $entityPingable->id;
        }

        $result = Heartbeat::whereIn('entity_pingable_id', $heartbeats);
        return $this->restrictResult($result, $from, $to);
    }

    /**
     * Get entity measurables relation for current context
     * @return Collection
     */
    final protected function entityMeasurables(): Collection
    {
        if (! isset($this->entityMeasurables))
        {
            $this->entityMeasurables = $this->hasMany(Entity\Measurable::class)->get();
        }

        return $this->entityMeasurables;
    }

    final protected function entityPingables(): Collection
    {
        if (! isset($this->entityPingables))
        {
            $this->entityPingables = $this->hasMany(Entity\Pingable::class)->get();
        }

        return $this->entityPingables;
    }

    /**
     * Restrict result given by the datetime objects
     * @param Builder $builder
     * @param \DateTime|null $form
     * @param \DateTime|null $to
     * @return Collection
     */
    final protected function restrictResult(Builder $builder, \DateTime $from = null, \DateTime $to = null): Collection
    {
        if (! is_null($from))
        {
            $builder->whereDate('timestamp', '>=', $from);
        }

        if (! is_null($to))
        {
            $builder->whereDate('timestamp', '<', $to);
        }

        return $builder->get();
    }
}