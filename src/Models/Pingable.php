<?php

namespace Monitor\Models;

use Illuminate\Database\Eloquent\Model;

class Pingable extends Model
{
    /**
     * Table name for model
     * @var string
     */
    public $table = 'pingable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'display_name'];
}