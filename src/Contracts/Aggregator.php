<?php

namespace Monitor\Contracts;

interface Aggregator
{
    /**
     * Get total from aggregation
     * @return float
     */
    public function get();
}
