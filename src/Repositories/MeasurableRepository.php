<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;
use Monitor\Models\Entity;
use Monitor\Models\Measurable;

class MeasurableRepository extends AbstractRepository
{
    use CacheableRetrieveRepository;

    /**
     * Find Measureable for entity
     * @param Entity $entity
     * @param string $code
     * @return Entity\Measurable
     * @throws \Exception
     */
    final public function findOrCreateForEntity(Entity $entity, string $code): Entity\Measurable
    {
        $measurable = $this->findOrCreateByCode($code);
        return Entity\Measurable::firstOrCreate([
            'entity_id' => $entity->id,
            'measurable_id' => $measurable->id
        ]);
    }

    /**
     * Find Measureable by code
     * @param string $code
     * @return Measurable
     * @throws \Exception
     */
    final public function findOrCreateByCode(string $code): Measurable
    {
        $code = strtolower($code);
        return $this->findOrCreateCached(
            ['code' => $code]
        );
    }

    /**
     * Context for this repository
     * @return Model
     */
    final protected static function context():Model
    {
        return new Measurable();
    }
}