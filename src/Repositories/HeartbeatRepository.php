<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;
use Monitor\Models\Entity\Pingable;
use Monitor\Models\Entity\Pingable\Heartbeat;

class HeartbeatRepository extends AbstractRepository
{
    /**
     * Find Measurement by external ID
     * @param Entity\Measurable $measurable
     * @param string $code
     * @return Heartbeat
     */
    final public function findOrCreateByTimestamp(Pingable $measurable, \DateTime $timestamp): Heartbeat
    {
        return static::context()->firstOrNew([
            'timestamp' => $timestamp,
            'entity_pingable_id' => $measurable->id
        ]);
    }


    /**
     * Context for this repository
     * @return Model
     */
    final protected static function context():Model
    {
        return new Heartbeat();
    }
}