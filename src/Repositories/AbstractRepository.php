<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;
use Monitor\Exceptions\InvalidArgumentException;

abstract class AbstractRepository
{
    /**
     * Find all records in database
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAll()
    {
        return static::context()->all();
    }

    /**
     * We want to follow the SRP, and make models only responsible for
     * data storage. Saving the model, would therfore become the
     * responsibility of the repository.
     *
     * @param Model $model
     * @return bool
     */
    public function save( Model $model ): bool
    {
        if(! is_a($model, static::context()))
        {
            throw new InvalidArgumentException(sprintf('Given model is not of type %s.', get_class(static::context())));
        }

        // Save the given model
        return $model->save();
    }

    /**
     * Bind model to its repository
     *
     * @return Model
     */
    abstract protected static function context(): Model;
}