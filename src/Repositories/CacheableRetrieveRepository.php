<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;

trait CacheableRetrieveRepository
{
    /**
     * Local index of retrieved entities
     *
     * @var Entity[]
     */
    protected $entities = [];

    /**
     * Get, create or retrieve from local properties
     * @param array $query
     * @param array $defaults
     * @return Model
     */
    final protected function findOrCreateCached(array $query, array $defaults = []): Model
    {
        $key = crc32(serialize($query));
        if( array_key_exists($key, $this->entities))
        {
            return $this->entities[$key];
        }

        $entity = static::context()
            ->firstOrCreate($query, $defaults);

        $this->entities[$key] = $entity;
        return $entity;
    }
}