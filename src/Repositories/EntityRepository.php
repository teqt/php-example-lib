<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;
use Monitor\Models\Entity;
use Ramsey\Uuid\Uuid;

class EntityRepository extends AbstractRepository
{
    use CacheableRetrieveRepository;

    /**
     * Get or create entity by name
     * @param string $name
     * @return Entity
     * @throws \Exception
     */
    final public function findOrCreateByName(string $name): Entity
    {
        return $this->findOrCreateCached(
            ['display_name' => $name],
            ['uuid' => Uuid::uuid4()->toString()]
        );
    }

    /**
     * Context for this repository
     * @return Model
     */
    final protected static function context():Model
    {
        return new Entity();
    }
}