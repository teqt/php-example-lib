<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;
use Monitor\Models\Entity;
use Monitor\Models\Pingable;

class PingableRepository extends AbstractRepository
{
    use CacheableRetrieveRepository;

    /**
     * Find Measureable for entity
     * @param Entity $entity
     * @param string $code
     * @return Entity\Pingable
     * @throws \Exception
     */
    final public function findOrCreateForEntity(Entity $entity, string $code): Entity\Pingable
    {
        $pingable = $this->findOrCreateByCode($code);
        return Entity\Pingable::firstOrCreate([
            'entity_id' => $entity->id,
            'pingable_id' => $pingable->id
        ]);
    }

    /**
     * Find Pingable by code
     * @param string $code
     * @return Pingable
     * @throws \Exception
     */
    final public function findOrCreateByCode(string $code): Pingable
    {
        $code = strtolower($code);
        return $this->findOrCreateCached(
            ['code' => $code]
        );
    }

    /**
     * Context for this repository
     * @return Model
     */
    final protected static function context():Model
    {
        return new Pingable();
    }
}