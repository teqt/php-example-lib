<?php

namespace Monitor\Repositories;

use Illuminate\Database\Eloquent\Model;
use Monitor\Models\Entity\Measurable;
use Monitor\Models\Entity\Measurable\Measurement;

class MeasurementRepository extends AbstractRepository
{
    /**
     * Find Measurement by external ID
     * @param Entity\Measurable $measurable
     * @param string $code
     * @return Measurement
     */
    final public function findOrCreateByTimestamp(Measurable $measurable, \DateTime $timestamp): Measurement
    {
        return static::context()->firstOrNew([
            'timestamp' => $timestamp,
            'entity_measurable_id' => $measurable->id
        ]);
    }


    /**
     * Context for this repository
     * @return Model
     */
    final protected static function context():Model
    {
        return new Measurement();
    }
}