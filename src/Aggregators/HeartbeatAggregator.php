<?php

namespace Monitor\Aggregators;

use Monitor\Contracts\Aggregator;
use Monitor\Models\Entity\Pingable\Heartbeat;

abstract class HeartbeatAggregator implements Aggregator
{
    /**
     * @var array
     */
    protected $heartbeats = [];

    /**
     * Add measurement to aggregation
     * @param Measurement $measurement
     * @return TotalAggregator
     */
    final public function add(Heartbeat $heartbeat)
    {
        $this->heartbeats[] = $heartbeat;
        return $this;
    }
}
