<?php

namespace Monitor\Aggregators;

use Monitor\Contracts\Aggregator;

class HourlyAggregator extends MeasurementAggregator implements Aggregator
{
    /**
     * Get total from aggregation
     * @return float
     */
    final public function get()
    {
        $result = [];
        foreach ($this->measurements as $measurement)
        {
            $hour = $measurement->timestamp->format('G');
            if(! array_key_exists($hour, $result))
            {
                $result[ $hour ] = 0;
            }

            $result[ $hour ] += $measurement->value;
        }

        return $result;
    }
}
