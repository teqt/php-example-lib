<?php

namespace Monitor\Aggregators;

use Monitor\Contracts\Aggregator;

class ThresholdAggregator extends MeasurementAggregator implements Aggregator
{
    /**
     * The thresholds to aggregate for
     * @var array
     */
    protected $thresholds;

    /**
     * ThresholdAggregator constructor.
     * @param array $thresholds
     */
    final public function __construct(array $thresholds = [])
    {
        $this->thresholds = $thresholds;
    }

    /**
     * Get total from aggregation
     * @return float
     */
    final public function get()
    {
        asort($this->thresholds);

        $totals = array_combine($this->thresholds, array_fill(0, count($this->thresholds), 0));
        $lastContinuesWeight = 0;
        $lastContinuesThreshold = null;
        foreach ($this->measurements as $measurement)
        {
            $lastThreshold = 0;
            foreach ($this->thresholds as $threshold)
            {
                if (floatval($measurement->value) > $threshold)
                {
                    $lastThreshold = $threshold;
                    continue;
                }

                if (! is_null($lastContinuesThreshold) && $lastThreshold !== $lastContinuesThreshold)
                {
                    if(! array_key_exists($lastContinuesThreshold, $totals) || $totals[$lastContinuesThreshold] < $lastContinuesWeight)
                    {
                        $totals[$lastContinuesThreshold] = $lastContinuesWeight;
                    }

                    $lastContinuesWeight = 0;
                }

                $lastContinuesWeight += $measurement->weight;
                $lastContinuesThreshold = $lastThreshold;
            }
        }

        return $totals;
    }
}
