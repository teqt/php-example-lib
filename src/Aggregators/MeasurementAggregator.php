<?php

namespace Monitor\Aggregators;

use Monitor\Models\Entity\Measurable\Measurement;

abstract class MeasurementAggregator
{
    /**
     * @var array
     */
    protected $measurements = [];

    /**
     * Add measurement to aggregation
     * @param Measurement $measurement
     * @return TotalAggregator
     */
    final public function add(Measurement $measurement)
    {
        $this->measurements[] = $measurement;
        return $this;
    }
}
