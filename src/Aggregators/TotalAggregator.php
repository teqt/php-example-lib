<?php

namespace Monitor\Aggregators;

use Monitor\Contracts\Aggregator;

class TotalAggregator extends MeasurementAggregator implements Aggregator
{
    /**
     * Get total from aggregation
     * @return float
     */
    final public function get()
    {
        $total = 0;
        foreach ($this->measurements as $measurement)
        {
            $total += $measurement->value;
        }

        return $total;
    }
}
