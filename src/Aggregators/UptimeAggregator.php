<?php

namespace Monitor\Aggregators;

use Monitor\Contracts\Aggregator;

/**
 * Class UptimeAggregator
 *
 * Assume all pings to be a kind of uptime. With this
 * aggregator, we calculate an uptime percentage.
 *
 * @package Monitor\Aggregators
 */
class UptimeAggregator extends HeartbeatAggregator implements Aggregator
{
    /**
     * Get total from aggregation
     * @return float
     */
    final public function get()
    {
        usort($this->heartbeats, function($a, $b) {
            if($a->timestamp->getTimestamp() === $b->timestamp->getTimestamp())
            {
                return 0;
            }

            return ($a->timestamp->getTimestamp() < $b->timestamp->getTimestamp() ? -1 : 1);
        });

        $up = $down = 0;
        $lastHeartbeat = null;
        foreach ($this->heartbeats as $heartbeat)
        {
            if(! $lastHeartbeat)
            {
                $lastHeartbeat = $heartbeat;
                continue;
            }

            $period = ($heartbeat->timestamp->getTimestamp() - $lastHeartbeat->timestamp->getTimestamp());
            if($lastHeartbeat->value)
            {
                $up += $period;
            }
            else
            {
                $down += $period;
            }

            $lastHeartbeat = $heartbeat;
        }

        // Prevent division by zero
        if(! $up)
        {
            return 1;
        }

        return $down / $up;
    }
}
