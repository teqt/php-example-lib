# Example Laravel poject

This package is created in addition to my example laravel project, located here: https://bitbucket.org/teqt/php-example . Please refer that project to read more about this project.

## Getting Started

### Installing

Add this repository to your composer file, and require the project as needed.

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:teqt/php-example-lib.git"
        }
    ]
}
```

```
~# composer require teqt/monitor
```
